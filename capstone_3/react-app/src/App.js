import './App.css';
import AppNavbar from "./components/AppNavbar";
/*import Banner from "./components/Banner"
import Highlights from "./components/Highlights"*/
import Home from "./pages/Home"
import Products from "./pages/Products"
import Register from "./pages/Register"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error"
import ProductView from "./components/ProductView"
import AdminDashboard from "./pages/AdminDashboard.js"



// npm install react-router-dom
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"


function App() {

  // Creating a user state for global scope

  const [user, setUser] = useState({
      id: null,
      isAdmin: localStorage.getItem("isAdmin"),
      email: localStorage.getItem("email"),
      token: localStorage.getItem("token")
  })

  // Function for clearing the storage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/productView/:productId" element={<ProductView/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="/admin-dashboard" element={<AdminDashboard/>}/>
                  <Route path="*" element={<Error/>}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
