const productsData = [
    {
        id: "app001",
        name: "iPhone 11 128GB",
        description: "Experience the perfect balance of performance, affordability, and user-friendly features with the iPhone 11, featuring a powerful chip, impressive camera capabilities, and a sleek design.",
        price: 25990,
        onOffer: true
    },
    {
        id: "app002",
        name: "iPhone 14 Pro Max 128GB",
        description: "Experience the pinnacle of innovation and luxury with the iPhone 14 Pro Max, featuring a stunning display, powerful performance, advanced camera system, and seamless connectivity for an unparalleled user experience.",
        price: 77990,
        onOffer: true
    },
    {
        id: "app003",
        name: "Macbook Air M2 512GB",
        description: "Unleash your productivity and creativity with the MacBook Air M2, powered by the next-generation M2 chip, delivering exceptional performance, stunning graphics, and all-day battery life in a sleek and portable design.",
        price: 87990,
        onOffer: true
    },
    {
        id: "app004",
        name: "Macbook Pro (14-inch) M2 Pro 512GB",
        description: "Experience unparalleled power and performance with the MacBook Pro M2 Pro, equipped with the cutting-edge M2 Pro chip, a stunning Retina display, advanced graphics, and a range of professional features for demanding tasks and creative endeavors.",
        price: 126990,
        onOffer: true
    },
    {
        id: "app005",
        name: "iPad Air 256GB",
        description: "Elevate your productivity and entertainment with the iPad Air, featuring a sleek design, powerful performance, and a stunning Liquid Retina display that brings your content to life.",
        price: 49990,
        onOffer: true
    },
    {
        id: "app006",
        name: "iPad Pro (11-inch) 256GB",
        description: "Unleash your creativity and productivity with the iPad Pro, equipped with the powerful M1 chip, a breathtaking Liquid Retina XDR display, and advanced features that push the boundaries of what a tablet can do.",
        price: 62990,
        onOffer: true
    },
    {
        id: "app007",
        name: "Apple Watch Series 8 45mm",
        description: "Experience the future of wearable technology with the Apple Watch Series 8, featuring a sleek design, advanced health tracking capabilities, and innovative features that seamlessly integrate into your active lifestyle.",
        price: 27990,
        onOffer: true
    },
    {
        id: "app008",
        name: "Apple Watch Ultra",
        description: "Stay connected, track your health, and enhance your daily life with the Apple Watch Ultra, featuring cutting-edge technology, a stunning display, and a range of advanced features that empower you to reach new heights of performance and wellness.",
        price: 52990,
        onOffer: true
    },
    {
        id: "app009",
        name: "Airpods Pro",
        description: "Immerse yourself in a world of rich audio and seamless connectivity with the Apple AirPods Pro, delivering active noise cancellation, customizable fit, and superior sound quality in a sleek and compact design.",
        price: 14990,
        onOffer: true
    },
    {
        id: "app010",
        name: "Airpods Max",
        description: "Experience audio like never before with the Apple AirPods Max, combining high-fidelity sound, adaptive noise cancellation, and luxurious comfort in a premium over-ear design.",
        price: 30990,
        onOffer: true
    },
    {
        id: "app011",
        name: "Apple Pencil",
        description: "Enhance your iPad experience with the Apple Pencil, a powerful and precise tool for seamless note-taking, drawing, and design.",
        price: 7990,
        onOffer: true
    }
]

export default productsData;

