import { Row, Col, Card, Image } from "react-bootstrap";

export default function Hightlights(){
	return(
		<Row className="mt-3 mb-3">
			{/*First Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body className="text-center">
						<Image
						className="d-block mx-auto img-card carousel-height carousel-caption-bg"
						src={require('../images/trade.jpg')}
						fluid
						/>
						<Card.Title className="mt-5">Trade In</Card.Title>
						<Card.Text>Apple program that enables customers to trade in their old devices for credit towards new Apple products. It offers a convenient and environmentally friendly way to upgrade devices while reducing electronic waste. Accepted products include iPhones, iPads, Macs, and Apple Watches. The trade-in process can be completed online or in Apple retail stores, making it easily accessible for customers.</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			{/*Second Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body className="text-center">
						<Image
						className="d-block mx-auto img-card carousel-height carousel-caption-bg"
						src={require('../images/education.jpg')}
						fluid
						/>
						<Card.Title>Apple for Education</Card.Title>
						<Card.Text>Apple Inc.'s initiative dedicated to enhancing learning experiences in educational settings. It offers a range of tailored hardware and software solutions for students, teachers, and educational institutions. With innovative products like Mac computers, iPads, and educational apps, Apple for Education aims to foster creativity, collaboration, and personalized learning in classrooms.</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			{/*Third Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body className="text-center">
						<Image
						className="d-block mx-auto img-card carousel-height carousel-caption-bg"
						src={require('../images/business.jpg')}
						fluid
						/>
						<Card.Title>Apple for Business</Card.Title>
						<Card.Text>A program by Apple Inc. that caters to the needs of businesses and enterprises. It provides a range of products, services, and solutions tailored to enhance productivity and efficiency in the workplace. From powerful Mac computers to innovative software and specialized services, Apple for Business aims to empower organizations and support their business needs.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}