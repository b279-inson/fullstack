import { Row, Col, Card, Button } from 'react-bootstrap';
import { useState, useEffect } from "react"
import {Link} from "react-router-dom"


export default function ProductCard({productProp}) {
    // Checks if props was successfully passed
    console.log(productProp.name);
    // checks the type of the passed data
    console.log(typeof productProp);

    // Destructuring the productProp into their own variables
    const { _id, name, description, price } = productProp;
    const [image, setImage] = useState("");

    useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        const activeProducts = data.filter((product) => product.isActive);

        setImage(data.image);
      });
  }, []);

    return (
        <Row className="mt-3 mb-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Img className="mx-auto " src={image} style={{ width: '50%', height: 'auto' }}/>
                    <Card.Body className="my-3 productCard">
                        <Card.Title className="pb-2">{name}</Card.Title>
                        <Card.Text>{description}</Card.Text>
                        <Card.Text>Php {price}</Card.Text>
                        <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>


        
    )
}
