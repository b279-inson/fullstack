import React from 'react';
import { MDBFooter, MDBContainer, MDBCol, MDBRow } from 'mdb-react-ui-kit';

export default function App() {
  return (
    <MDBFooter bgColor='light' className='text-lg-left mt-5'>
      <MDBContainer className='p-4'>
        <MDBRow>
          <MDBCol lg='6' md='12' className='mb-4 mb-md-0'>
            <h5>ABOUT iSHOP</h5>

            <p className="text-align-left">
              iShop is an Apple Premium Reseller, established on May 22, 2023 with 10 retail locations in major cities in the Luzon region.
            </p>
          </MDBCol>

          <MDBCol lg='6' md='12' className='mb-4 mb-md-0'>
            <h5 className='text-uppercase'>Our Mission and Core Purpose</h5>

            <p>
            	iShop aims to enrich lives through quality products, providing the unique Apple experience. Our Apple-certified staff offer expert knowledge, our stores feature beautiful designs for a complete Apple experience, and we provide all the accessories and services you need for a seamless digital lifestyle.
            </p>
          </MDBCol>
        </MDBRow>
      </MDBContainer>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
     	 © iShop. Apple Premium Reseller{' '}
      </div>
    </MDBFooter>
  );
}