import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel() {
return (
    <div >
    <Carousel className='vh-75 w-100 d-inline-block mb-5 shadow' fade>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/iphone.jpg')}
        fluid
        />
        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25 '>
        <div  className='carousel-text'>
        <h1>iPhone</h1>
        <p>Pro. Beyond. Wonderful.</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/ipad.jpg')}
        fluid
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>iPad</h1>
        <p>Lovable. Drawable. Magical.</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/macbook2.jpg')}
        fluid
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Macbook</h1>
        <p>
            Mover. Maker. Boundary breaker.
        </p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    </div>
);
}
