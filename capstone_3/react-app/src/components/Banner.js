// object destructuring
import { Row, Col, Button, Image } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({bannerProps}){
	console.log(bannerProps);

	const {title, content, destination, label, image} = bannerProps;

	return(
			<div className="vh-100 banner-bg text-light d-flex align-items-center justify-content-center mb-5 mt-5">
			<Row>
				<Col className="text-center d-flex flex-column justify-content-center align-items-center" lg={12}>
                <Image
                className="d-block w-50  carousel-height circle-crop"
                src={require('../images/ishop-logo4.png')}
                fluid
                />
					{/*<h1>{title}</h1>*/}
					<p>{content}</p>
					<Button as={Link} to={destination} className="btn-secondary">{label}</Button>
				</Col>
			</Row>
			</div>
		);
}