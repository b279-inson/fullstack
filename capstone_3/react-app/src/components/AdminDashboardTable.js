import { Card, Button, Form, Modal, Image } from "react-bootstrap"
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom"
import Table from 'react-bootstrap/Table';
import Swal from "sweetalert2"






export default function AdminDashboardTable({productProp}){
	// Checks if props was successfully passed
	console.log(productProp.name);
	// Checks the type of the passed data
	console.log(typeof productProp);


	// Destructuring the courseProp into their own variables
	const { _id, name, description, price, isActive, image } = productProp;
	const { productId } = useParams();


	const [archive, setIsArchive] = useState("")
	const [unarchive, setIsUnarchive] = useState("")

	const [showModal, setShowModal] = useState(false);
	const [modalName, setModalName] = useState(name);
	const [modalDescription, setModalDescription] = useState(description);
	const [modalPrice, setModalPrice] = useState(price);
	const [modalIsActive, setModalIsActive] = useState(isActive);
	const [modalImage, setModalImage] = useState(image);

	




		// Update AND Delete (Archive/Unarchive) product
		
		const handleProductUpdate = (e) => {
			e.preventDefault();


			fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			    method: "PUT",
			    headers: {
			        'Content-Type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem("token")}`
			    },
			    body: JSON.stringify({
			        name: modalName,
			        description: modalDescription,
			        price: modalPrice,
			        isActive: modalIsActive,
			        image: modalImage
			    }),
			})
			.then(res => res.json())
			.then(data => {

			    console.log(data);

			            if(data === true){

			                // Clear input fields
			                setModalName('');
			                setModalDescription('');
			                setModalPrice('');
			                setModalIsActive(false);
			                setModalImage('');



			                setShowModal(false);

			                Swal.fire({
			                    title: 'Successfully updated!',
			                    icon: 'success',
			                    text: 'The product has been updated.',
			                }).then(() => {
		                	window.location.reload();
		                })
			                setShowModal(true)

			            } else {

			                Swal.fire({
			                    title: 'Something is wrong!',
			                    icon: 'error',
			                    text: 'Please try again.'   
			                });

			            };

			        })
					

			    };

			    const handleUpdateProduct = () => {
			    	setModalIsActive(isActive);
			    	setShowModal(true)
			    };


	return (

			<>
			<Table striped bordered hover responsive>
			    <thead>
			    	<tr>
			    		<th className="text-center">Product</th>
			    		<th className="text-center">Description</th>
			    		<th className="text-center">Price</th>
			    		<th className="text-center">Active</th>
			    		<th></th>
			    	</tr>
			    </thead>
			    <tbody>
			        <tr>
			          <td className="text-center p-5">{name}</td>
			          <td className="pt-5">{description}</td>
			          <td className="text-center pt-5">Php {price}</td>
			          <td className="text-center pt-5"> {isActive.toString()}</td>
			          <td className="pt-5">
			          	<Button variant="secondary" className="my-1" type="button" id="submitBtn" onClick={handleUpdateProduct}>
		          		  Edit
		          		</Button>
			          </td>
			        </tr>
			    </tbody>
			  </Table>

			  <Modal show={showModal} onHide={() => setShowModal(false)} className="modal-dashboard" contentClassName="modal-content-dashboard">
			    <Modal.Header closeButton>
			      <Modal.Title>Edit Product</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			      <Form>
			        <Form.Group controlId="name">
			          <Form.Label>Name</Form.Label>
			          <Form.Control			    
			            type="text"
			            placeholder="Enter product name"
			            value={modalName}
			            onChange={(e) => setModalName(e.target.value)}
			            required
			          />
			        </Form.Group>
			        <Form.Group controlId="description">
			          <Form.Label className="pt-3">Description</Form.Label>
			          <Form.Control
			            as="textarea"
			            rows={6}
			            placeholder="Enter product description"
			            value={modalDescription}
			            onChange={(e) => setModalDescription(e.target.value)}
			            required
			          />
			        </Form.Group>
			        <Form.Group controlId="price">
			          <Form.Label className="pt-3">Price</Form.Label>
			          <Form.Control
			            type="number"
			            placeholder="Enter product price"
			            value={modalPrice}
			            onChange={(e) => setModalPrice(e.target.value)}
			            required
			          />
			        </Form.Group>

			        <Form.Group className="pt-3" controlId="isActive">
			               <Form.Check
			                 type="checkbox"
			                 label="Active"
			                 checked={modalIsActive}
			                 onChange={(e) => setModalIsActive(e.target.checked)}
			               />
			        </Form.Group>

			        <Form.Group controlId="image">
			            <Form.Label className="pt-3">Image Link</Form.Label>
			            <Form.Control
			                type="text"
			                placeholder="Enter Product Image Link"
			                value={modalImage}
			                onChange={e => setModalImage(e.target.value)}
			            />
			        </Form.Group>
			        

			        <Modal.Footer className="pt-4">
			          <Button variant="danger" onClick={() => setShowModal(false)}>
			            Close
			          </Button>
			          <Button variant="primary" onClick={handleProductUpdate}>
			            Save
			          </Button>
			        </Modal.Footer>
			      </Form>
			    </Modal.Body>
			  </Modal>
			</>

		  );
}

