import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Row, Col} from "react-bootstrap"
import {useParams} from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function ProductView(){

	const { user, setUser } = useContext(UserContext);

	// module that allows us to retrieve the productId passed via URL
	const {productId} = useParams();

	const [quantity, setQuantity] = useState(1);
	const handleQuantityChange = (event) => {
		setQuantity(event.target.value);
	};

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [image, setImage] = useState("")
	const storedIsAdmin = localStorage.getItem('isAdmin');


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);
		})

	}, [productId])

	// User checkout (purchase)
	const purchase = (productId) => {

		const totalAmount = price * quantity;

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
			}),
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


			if(storedIsAdmin){
				Swal.fire({
					title: "Congratulations! Your order was placed!",
					icon: 'success',
					text: "Thank you for your purchase."
				});
			}
			else{
					Swal.fire({
						title: "Purchase Failed!",
						icon: 'error',
						text: "Purchases can only be made by customers who are logged in."
						});
				
				}
			})
		}

	useEffect(() => {
	   const token = localStorage.getItem("token");
	   if (token) {
	     retrieveUserDetails(token);
	   }
	 }, []);

	 // Retrieve user details using its token
	const retrieveUserDetails = (token) => {
	    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	    	method: "GET",
	        headers: {
	            Authorization: `Bearer ${token}`
	        }
	    })
	    .then(res => res.json())
	    .then(data => {
	        setUser({
	            id: data._id,
	            isAdmin: data.isAdmin,
	            email: data.email
	        })
	        localStorage.setItem("isAdmin", data.isAdmin);
	        console.log(data)
	    })
	}


	return(
		<Container className="mt-5 mb-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="p-5">
						<Card.Img className="mx-auto " src={image} style={{ width: '50%', height: 'auto' }}/>
						<Card.Body className="text-center">
							<Card.Title className="pb-3">{name}</Card.Title>
							{/*<Card.Subtitle className="pb-1">Description:</Card.Subtitle>*/}
							<Card.Text className="pb-3">{description}</Card.Text>
							{/*<Card.Subtitle className="pb-1">Price:</Card.Subtitle>*/}
							<Card.Subtitle className="pb-4">Php {price}</Card.Subtitle>
							<Card.Text>Quantity:</Card.Text>
				            <Card.Text className="pb-2">
				               <input type="number" min="1" value={quantity} onChange={handleQuantityChange}/>
				            </Card.Text>

							<Button variant="primary" onClick={() => purchase(productId)}>Buy</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		);
}