import {Nav, Navbar, NavDropdown, Container} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { useState, useContext, useEffect } from "react"
import UserContext from "../UserContext"
import React from 'react';


export default function AppNavbar(){

  // State to store user info stored in the login page
  
  const { user } = useContext(UserContext);

  console.log('user.token:', user.token);
  console.log('user.isAdmin:', user.isAdmin);

  console.log(user)

  const storedToken = localStorage.getItem('token');
  const storedIsAdmin = localStorage.getItem('isAdmin');
  const storedEmail = localStorage.getItem('email')

 
  return(
  <Navbar bg="dark" expand="lg" className="px-3 shadow sticky-top">
    <Container fluid>
      <img
            alt=""
            src={require('../images/ishop-logo.png')}
            width="30"
            height="30"
            className="d-inline-block align-top rounded"
            />{' '}
      <Navbar.Brand as={Link} to={"/"} className="fw-bold text-light ms-3">
      iShop
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          
          {

            storedToken && storedIsAdmin === "true" ? (
                <>
                  <span className="text-light ps-1 pt-2 pe-4 loggedInAs">[Logged in as: ADMIN]</span>
                  <Nav.Link as={NavLink} to={"/"} className="text-light">Dashboard</Nav.Link>
                  <Nav.Link as={NavLink} to={"/logout"} className="text-light">Logout</Nav.Link>
                </>
              ) : storedToken ? (
                <>
                  <span className="text-light ps-1 pt-2 pe-4 loggedInAs">[Logged in as: {storedEmail}]</span>
                  <Nav.Link as={NavLink} to={"/"} className="text-light">Home</Nav.Link>
                  <Nav.Link as={NavLink} to={"/products"} className="text-light">Products</Nav.Link>
                  <Nav.Link as={NavLink} to={"/logout"} className="text-light">Logout</Nav.Link>
                </>

              ) : (
                <>
                  <Nav.Link as={NavLink} to={"/"} className="text-light">Home</Nav.Link>
                  <Nav.Link as={NavLink} to={"/products"} className="text-light">Products</Nav.Link>
                  <Nav.Link as={NavLink} to={"/register"} className="text-light">Register</Nav.Link>
                  <Nav.Link as={NavLink} to={"/login"} className="text-light">Login</Nav.Link>
                </>

              )

          }

        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
    )
}