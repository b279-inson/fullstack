import { Form, Button, Modal, Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext.js";
import {useNavigate, Navigate} from "react-router-dom";
import Swal from 'sweetalert2';
import AdminDashBoardTable from "../components/AdminDashboardTable.js";



export default function Dashboard(){
	const { user } = useContext(UserContext);
	const [AllProducts, setAllProducts] = useState([]);
	const [showModal, setShowModal] = useState(false);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState("");

	const storedToken = localStorage.getItem('token');
	const storedIsAdmin = localStorage.getItem('isAdmin');


	// Retrieve all products
	const fetchAllProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(
				data.map((product) => {
				return (
					<AdminDashBoardTable key={product._id} productProp={product}/>
					)
				})
			)

		})
	}

	useEffect(() => {
		fetchAllProducts();
	}, []);
	

	// Create product
	const handleCreateProduct = () => {
		setShowModal(true)
	};

	const handleProductCreation = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        name: name,
		        description: description,
		        price: price,
		        isActive: isActive
		    }),
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);
		            if(data === true){

		                // Clear input fields
		                setName('');
		                setDescription('');
		                setPrice('');
		                setIsActive('');

		                setShowModal(false);

		                Swal.fire({
		                    title: 'Product Created',
		                    icon: 'success',
		                    text: 'You have created the product',
		                }).then(() => {
		                	window.location.reload();
		                })

		                fetchAllProducts();
		            } else {

		                Swal.fire({
		                    title: 'Something wrong',
		                    icon: 'error',
		                    text: 'Please try again.'   
		                });

		            };

		        })
		    };


return(


	<>

	     {storedToken && storedIsAdmin === "true" ? (
	       <>
	         <h1 className="dashboardHeader pt-3"></h1>
	         <div className="py-3">
	           <Row>
	           	<Col className="justify-content-center align-items-center ">
	           		<Button  variant="primary" size="md" onClick={handleCreateProduct}>
	           		  Create a New Product
	           		</Button>
	           	</Col>
	           </Row>
	         </div>
	         {AllProducts}

	         <Modal show={showModal} onHide={() => setShowModal(false)} className="modal-dashboard">
	           <Modal.Header closeButton>
	             <Modal.Title>Create Product</Modal.Title>
	           </Modal.Header>
	           <Modal.Body>
	             <Form>
	               <Form.Group controlId="name">
	                 <Form.Label>Name</Form.Label>
	                 <Form.Control
	                   type="text"
	                   placeholder="Enter product name"
	                   value={name}
	                   onChange={(e) => setName(e.target.value)}
	                   required
	                 />
	               </Form.Group>
	               <Form.Group controlId="description">
	                 <Form.Label>Description</Form.Label>
	                 <Form.Control
	                   type="text"
	                   placeholder="Enter product description"
	                   value={description}
	                   onChange={(e) => setDescription(e.target.value)}
	                   required
	                 />
	               </Form.Group>
	               <Form.Group controlId="price">
	                 <Form.Label>Price</Form.Label>
	                 <Form.Control
	                   type="number"
	                   placeholder="Enter product price"
	                   value={price}
	                   onChange={(e) => setPrice(e.target.value)}
	                   required
	                 />
	               </Form.Group>
	               <Form.Group controlId="isActive">
	                 <Form.Check
	                   type="checkbox"
	                   label="Is Active"
	                   checked={isActive}
	                   onChange={(e) => setIsActive(e.target.checked)}
	                 />
	               </Form.Group>

	               <Modal.Footer>
	                 <Button variant="secondary" onClick={() => setShowModal(false)}>
	                   Close
	                 </Button>
	                 <Button variant="primary" onClick={handleProductCreation}>
	                   Create Product
	                 </Button>
	               </Modal.Footer>
	             </Form>
	           </Modal.Body>
	         </Modal>
	        



	       </>
	       








	     ) : (
	       <Navigate to="/" />
	     )}
	   </>
	
	)
	
}

