import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2"

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding API
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to iShop!',
                })

            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Login Failed!',
                  text: 'Your email or password is incorrect. Please try again.',
                })
            }
        })

        // setUser({
        //     // Store the email in the localStroge
        //     email: localStorage.setItem("email", email)
        // })

        // Clear input fields after submission
        setEmail('');
        setPassword('');

    }

     // Retrieve user details using its token
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                email: data.email
            })
            localStorage.setItem("isAdmin", data.isAdmin);
            localStorage.setItem("email", data.email);
            console.log(data)
        })
    }





    return (
        <>
            {user.token !== null ? (
                <Navigate to="/"/>
            ) : (

                <>
                    <Container fluid className="w-100 m-0 p-0">
                        <Row className="w-100 m-0 p-0">
                            <Col className="login-bg vh-100 m-0 text-light d-flex flex-column align-items-center justify-content-center text-center p-5" xs={12} md={12} lg={6}>
                                {/*<h1 className='display-1'>LOGIN NOW,</h1>
                                <h1 className='display-5'>Check some exciting gadgets on sale!</h1>*/}
                            </Col>
                            <Col xs={12} md={12} lg={6} className="d-flex flex-column align-items-center justify-content-center m-0 colr-bg ">
                            <div className='w-75 vh-50 p-5 shadow-sm shadow-lg rounded bg-white my-5 d-flex flex-column align-items-center justify-content-center'>
                            <h1 className="my-3 text-center pb-3">Login to my account</h1>
                            
                            <Form className="w-100 pb-5" onSubmit={(e) => authenticate(e)}>
                            <Form.Group controlId="userEmail" className="mb-3">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password" className="mb-3">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {
                            isActive
                            ?
                                <Button variant="primary" className='px-2 py-2 w-25' type="submit" id="submitBtn">
                                    Login
                                </Button>
                            :
                                <Button variant="danger" className='px-2 py-2 w-25' type="submit" id="submitBtn" disabled>
                                    Login
                                </Button>
                            }
                            <div className='d-flex flex-column align-items-center justify-content-center'>
                            <p className='mt-5'>Don't have an account? <Link to={"/register"}><strong>Sign up!</strong></Link></p>
                            </div>
                            </Form>
                            </div>
                            </Col>
                        </Row>
                    </Container>
                </>

            )}
        </> 
    )
}
