import { Form, Button, Col, Row, Container } from "react-bootstrap";
import { useState, useEffect, useContext  } from "react";
import UserContext from '../UserContext';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";



export default function Register(){

	// UserContext Here
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
    const [address, setAddress] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	

	useEffect(() => {
		// Validation to enable register button
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && address !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[firstName, lastName, email, mobileNo, address, password1, password2]);

	// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(address);
	console.log(password1);
	console.log(password2);


	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){

		        Swal.fire({
		            title: 'An email address duplication has been detected.',
		            icon: 'error',
		            text: 'Kindly provide a new email address.'   
		        });

		    } else {

		        fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
		            method: "POST",
		            headers: {
		                'Content-Type': 'application/json'
		            },
		            body: JSON.stringify({
		            	firstName: firstName,
		            	lastName: lastName,
		            	email: email,
		            	mobileNo: mobileNo,
		            	address: address,
		            	password1: password1

		            })
		        })
		        .then(res => res.json())
		        .then(data => {
		            console.log(data);

		            if(data === true){

		            	// clear input fields
		            	setFirstName("");
		            	setLastName("");
		            	setEmail("");
		            	setMobileNo("");
		            	setAddress("");
		            	setPassword1("");
		            	setPassword2("");

		            	// alert("Thank you for registering!");
		            	Swal.fire({
		            	    title: "You are now Registered!",
		            	    icon: "success",
		            	    text: "Kindly login to check your account."
		            	});

		            	// Allows us to redirect the user to the login page after registering for an account
	                       navigate("/login");

	                   } else {

	                       Swal.fire({
	                           title: 'An error has occurred.',
	                           icon: 'error',
	                           text: 'Please try again.'   
	                       });

	                   };

	               })
	           };

	       })

	}


	return(
		(user.token !== null) ?
			<Navigate to="/" />
		:
		
		<>
		    <Container fluid className="w-100 m-0 p-0">
		    <Row className="w-100 m-0 p-0">
		    
		   	<Col className="register-bg vh-150 m-0 text-light d-flex flex-column align-items-center justify-content-center text-center p-5" xs={12} md={12} lg={6}>
		        {/*<h1 className='display-1'>REGISTER NOW,</h1>
		        <h1 className='display-5'>We're sure you don't want to miss our surprises for you.</h1>*/}
		    </Col> 


		    <Col xs={12} md={12} lg={6} className="d-flex flex-column align-items-center justify-content-center m-0 colr-bg">
		        <div className='w-100 my-5 py-5 rounded bg-white d-flex flex-column align-items-center justify-content-center'>
		        <h1 className=" text-center pb-3">SIGN UP</h1>
		        
		        <Form className="w-75" onSubmit={e => registerUser(e)}>

		        <Form.Group className="mb-3" controlId="firstName">
		            <Form.Label>First Name</Form.Label>
		            <Form.Control
		                type="text"
		                placeholder="Enter first name"
		                value={firstName}
		                onChange={e => setFirstName(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="lastName">
		            <Form.Label>Last Name</Form.Label>
		            <Form.Control
		                type="text"
		                placeholder="Enter last name"
		                value={lastName}
		                onChange={e => setLastName(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="emailAddress">
		            <Form.Label>Email Address</Form.Label>
		            <Form.Control
		                type="email"
		                placeholder="Enter email"
		                onChange={e => setEmail(e.target.value)}
		                value={email}
		                required
		            />
		            <Form.Text className="text-muted">
		            We'll never share your email with anyone else.
		            </Form.Text>
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="mobileNo">
		            <Form.Label>Mobile Number</Form.Label>
		            <Form.Control
		                type="number"
		                placeholder="09xxxxxxxxx"
		                value={mobileNo}
		                onChange={e => setMobileNo(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="address">
		            <Form.Label>Address</Form.Label>
		            <Form.Control
		                type="text"
		                placeholder="Enter address"
		                value={address}
		                onChange={e => setAddress(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="password1">
		            <Form.Label>Password</Form.Label>
		            <Form.Control
		                type="password" 
		                placeholder="Enter Password"
		                value={password1}
		                onChange={e => setPassword1(e.target.value)}
		                required
		            />
		        </Form.Group>

		        <Form.Group className="mb-3" controlId="password2">
		            <Form.Label>Verify Password</Form.Label>
		            <Form.Control
		                type="password" 
		                placeholder="Verify Password"
		                value={password2}
		                onChange={e => setPassword2(e.target.value)}
		                required
		            />
		        </Form.Group>
		        {
		            isActive
		            ?
		                <Button variant="primary" type="submit" id="submitBtn">
		                Register
		                </Button>
		            :
		                <Button variant="danger" type="submit" id="submitBtn" disabled>
		                Register
		                </Button>
		        }
		        <div className='d-flex flex-column align-items-center justify-content-center'>
		        <p className='mt-5'>Already have an account? <Link to={"/login"}><strong>Login here!</strong></Link></p>
		        </div>
		        </Form>
		        </div>
		    </Col>

		    </Row>
		    </Container>
		</>
	
		);
}