import productData from "../data/products"
import ProductCard from '../components/ProductCard'
import { useEffect, useState, useContext} from "react"
import { Card, Form, Container, Row, Col, CardGroup } from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext.js";


export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [filterText, setFilterText] = useState("");
  const storedIsAdmin = localStorage.getItem('isAdmin');


	useEffect(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	      .then(res => res.json())
	      .then(data => {
	        console.log(data);

	        const activeProducts = data.filter((product) => product.isActive);

	        setProducts(activeProducts);
	        setFilteredProducts(activeProducts);
	      });
	  }, []);

	  const handleFilterChange = (event) => {
	    const searchText = event.target.value.toLowerCase();
	    setFilterText(searchText);

	    const filtered = products.filter((product) =>
	      product.name.toLowerCase().includes(searchText)
	    );

	    setFilteredProducts(filtered);
	  };

	return (
	  <>
	    {storedIsAdmin === 'true' ? (
	      <Navigate to="/admin-dashboard" />
	    ) : (
	      <>
	        <h1 className="text-center pt-5"></h1>
	        <Row className="my-3">
	           <Col className="my-2">
	              <Card className="cardHighlight p-3">
	              	<Card.Body>

	              		  <Row className="productRow">
	              		    {filteredProducts.map((product) => (
	              		       <Col key={product._id} sm={12} md={6} lg={4} className="mb-3">
	              		           
	              		             <ProductCard productProp={product} />
	              		           
	              		      </Col>
	              		    ))}
	              		  </Row>
	              		 
	              	</Card.Body>
	              </Card>
	            </Col>
	        </Row>
	      </>
	    )}
	    
	  </>
	);

}