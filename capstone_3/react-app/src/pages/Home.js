import Banner from "../components/Banner"
import logo from "../images/ishop-logo.png"
import Highlights from "../components/Highlights"
import Footer from "../components/Footer"
import UserContext from "../UserContext";
import HomeCarousel from "../components/HomeCarousel";

import { Navigate } from "react-router-dom";
import { useContext } from "react"


export default function Home(){

	const { user } = useContext(UserContext);
	const data = {
	title: "iShop",
	content: "Apple Premium Reseller",
	destination: "/products",
	label: "Shop Now",
	image: {logo}
	}

	return(
			<>
				{localStorage.getItem("isAdmin") === "true" ? 
				(
				<Navigate to="/admin-dashboard" />
				) : (
				<div>
					<Banner bannerProps={data} />
					<HomeCarousel/>
					<Highlights/>
					<Footer/>
				</div>
				
				)}
			</>
		)
}