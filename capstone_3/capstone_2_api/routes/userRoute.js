const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")


// route for checking if email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// route for user authentication/login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile(userData.id).then(resultFromController => res.send(resultFromController))
});


// route for non-admin user checkout (create order)

router.post("/checkout", auth.verify, (req, res) => {
	let userId = auth.decode(req.headers.authorization);
	let productId = req.body.productId;

	if(userId.isAdmin){
		res.send("Only customers are allowed to purchase.")
	}
	else{
		userController.purchase(userId, productId, req.body).then(resultFromController => res.send(resultFromController))
	}
	
})

// route for retrieving All Users
router.get("/all", (req, res) => {
	userController.getAllProfiles().then(resultFromController => res.send(resultFromController));
})




// STRETCH GOALS:

// Route for setting user as admin (Only admins can do this feature)
router.put("/:id/setUserAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.setUserAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


// route for retrieving authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
		const userId = auth.decode(req.headers.authorization).id;
  userController.getUserOrders(userId).then((resultFromController) => res.send(resultFromController));
});


// route for adding to cart
router.post("/addToCart", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization);
  const orders = req.body.orders;

  if (userId.isAdmin) {
    res.send("Only customers are allowed to purchase.");
  } else {
      const result = await userController.addToCart(userId, orders);
        res.send(result);
 	}
 });




// Allows us to export the "router" object that will be accessed in our index.js file.
module.exports = router;
