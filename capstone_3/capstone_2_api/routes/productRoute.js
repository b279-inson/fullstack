const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// Route for creating a product (admin only)
/*
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});
*/


router.post("/createProduct", (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	
});


router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})




// Get all products
/*
non-admin:

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})
*/



// Get all "ACTIVE" products
/*
non-admin:

router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})*/

/*
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})
*/


// Retrieve specific/single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})



// Update product information (Admin only)
router.put("/:productId", (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})



// Route to archiving a product (Admin only)
router.put("/:id/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.archiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Route to unarchiving a product (Admin only)
router.put("/:id/unarchive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.unarchiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})







// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;