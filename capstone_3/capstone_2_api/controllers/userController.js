const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")


// fuction to check if the email already exist

module.exports.checkEmailExist = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		}
		else {
			return false
		}
	});
}


// function to register a user


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		address: reqBody.address,
		password: bcrypt.hashSync(reqBody.password, 10)
		
	})
	
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})
};



// function to login a user

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})

}


// Retrieve User Details
module.exports.getProfile = (userData) => {
	return User.findById(userData).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.password = ""
			return result;
		}

	})

}



// function for non-admin user to checkout (create order)
/*
module.exports.userCheckout = async (data) => {

	if(data.isAdmin){
		return "ADMIN cannot do this action.";
	}else{

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orderedProduct.push({productId : data.productId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

		if(isUserUpdated && isProductUpdated){
			return "You have successfully purchased the product!";
		}else{
			return "Something went wrong with your request. Please try again later!";
		}
	}
};
*/


/*
module.exports.checkout = async (data, reqBody, isAdmin) => {
    console.log(data.isAdmin);

    if(data.isAdmin){
            return "You're not allowed to order!"
    }else{
        let isUserUpdated = await User.findById(data.userId).then(user => {
            return Product.findById(data.product.productId).then(result =>{
                console.log(result.name)
            let newOrder = {
                products : [{
                    productId : data.product.productId,
                    productName : result.name,
                    quantity : data.product.quantity
                }],
                totalAmount : result.price * data.product.quantity
            }
            user.orderedProduct.push(newOrder);


        console.log(data.product.productId);

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
            })

    })


    let isProductUpdated = await Product.findById(data.product.productId).then(product => {

        product.userOrders.push({userId: data.userId})

        return product.save().then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })

        })


    if(isUserUpdated && isProductUpdated){
        return "Sucessfully Ordered!";
    }else{
        return "Something went wrong with your request. Please try again later!";
    }
    }
}

*/


module.exports.purchase = async(userId, productId, reqBody) => {
	let user = await User.findById(userId.id);
	let product = await Product.findById(productId);
	const currentDate = new Date();


	const subtotal = reqBody.quantity * product.price;
	const newOrder = {
		products: [
			{
				productId: product._id,
				productName: product.name,
				description: product.description,
				quantity: reqBody.quantity,
				price: product.price
			}

		],
		purchasedOn: currentDate,
		totalAmount: subtotal
	}

	user.orderedProduct.push(newOrder);
	product.userOrders.push({
		userId: userId.id,
		totalAmount: subtotal, 
		purchasedOn: currentDate
	});

	let isUserUpdated = await user.save().then(isUserUpdated => isUserUpdated)
	let isProductUpdated = await product.save().then(isProductUpdated => isProductUpdated)

	if (isUserUpdated && isProductUpdated) {
        return true;
    } else {
        return false;
    }
}




// Retrieve all user details
module.exports.getAllProfiles = () => {
	return User.find({}).then(result => {
		return result;
	})
}



// STRETCH GOALS:

// SETTING user as admin (Admin only)

module.exports.setUserAdmin = (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
			let userToAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(reqParams.id, userToAdmin).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return "User converted to admin!";
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}


// Retrieving authenticated user's orders (ADMINS only)
module.exports.getUserOrders = (userId) => {
  return User.findById(userId).then((result) => {
    const products = [];
    const orderedProducts = result.orderedProduct;

    for (let i = 0; i < orderedProducts.length; i++) {
      const product = orderedProducts[i].products[0];
      products.push(product);
    }
    
    return products;
  });
}




// Add to Cart

module.exports.addToCart = async(userId, orders) => {
  let user = await User.findById(userId.id);
 	const currentDate = new Date();
  let products = [];
  let totalAmount = 0;

  for (let i = 0; i < orders.length; i++) {
    let product = await Product.findById(orders[i].productId);

    const newProduct = {
      productId: product._id,
      productName: product.name,
      price: product.price,
      quantity: orders[i].quantity
    }

    let subtotal = product.price * orders[i].quantity;

    user.orderedProduct.push({
    	products: [newProduct],
    	purchasedOn: currentDate,
    	price: product.price,
    	subtotal: subtotal,
    	quantity: orders[i].quantity
    })

    totalAmount = totalAmount + subtotal;
    products.push(newProduct);
    product.userOrders.push({
    	userId: userId.id,
    	totalAmount: subtotal,
    	purchasedOn: currentDate
    });
    await product.save();
  }

  // Add totalAmount to the last order object in the orderedProduct array
  user.orderedProduct[user.orderedProduct.length - 1].totalAmount = totalAmount;

  let isUserUpdated = await user.save();

  if (isUserUpdated) {
    return "Purchase successful";
  } else {
    return "Something went wrong with your request. Please try again later.";
  }
}





