const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "PRODUCT NAME is required!"]
	},
	description : {
		type : String,
		required : [true, "PRODUCT DESCRIPTION is required!"]
	},
	image: {
		type: String
	},
	price : {
		type : Number,
		required : [true, "PRODUCT PRICE is required!"]
	},
	quantity: {
		type: Number
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates the current date
		default : new Date()
	},
	userOrders : [
			{
				userId : {
					type : String,
					default: ""
				},
				purchasedOn : {
					type : Date,
					default: new Date()
				}
			}
		]
})


module.exports = mongoose.model("Product", productSchema);



