const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "FIRST NAME is required!"]
	},
	lastName : {
		type : String,
		required : [true, "LAST NAME is required!"]
	},
	email : {
		type : String,
		required : [true, "EMAIL is required!"]
	},
	password : {
		type : String,
		required : [true, "PASSWORD is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	address : {
		type : String,
		required : [true, "ADDRESS is required!"]
	},
	mobileNo : {
		type : String,
		required : [true, "MOBILE NUMBER is required!"]
	},
	orderedProduct : [
			{
				products : [
						{
							productId : {
								type : mongoose.Schema.Types.ObjectId
							},
							productName : {
								type : String,
							},
							description: {
								type: String,
							},
							price : {
								type : Number,
							},
							quantity : {
								type : Number,
							}
						}
					],
				totalAmount : {
					type : Number,
				},
				subtotal: {
					type: Number
				},
				purchasedOn : {
					type : Date,
					default : new Date ()
				}
			}
		]
})	




module.exports = mongoose.model("User", userSchema);
